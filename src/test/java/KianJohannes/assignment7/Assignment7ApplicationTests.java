package KianJohannes.assignment7;

import KianJohannes.assignment7.models.Movie;
import KianJohannes.assignment7.repositories.MovieRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class Assignment7ApplicationTests {

	@Autowired
	MovieRepository movieRepository;

	@Test
	void contextLoads() {
	}
}
