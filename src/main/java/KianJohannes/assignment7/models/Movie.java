package KianJohannes.assignment7.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "genre")
    private String genre;

    @Column(name = "release_year")
    private Date releaseYear;

    @Column(name = "picture")
    private String picture;

    @Column(name = "director")
    private String director;

    @Column(name = "trailer")
    private String trailer;

    public Movie(String title, String genre) {
        this.title = title;
        this.genre = genre;
    }

    public Movie() {

    }

    @ManyToOne
    @JoinColumn(
        name = "franchise_id"
    )
    public Franchise franchise;

    @ManyToMany
    @JoinTable(
            name = "character_movie",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "character_id")}

    )
    public Set<MovieCharacter> characters;

    @JsonGetter("franchise")
    public String franchise() {
        if (franchise != null) {
            return "/api/v1/franchises/" + franchise.getId();
        }
        return null;
    }

    @JsonGetter("characters")
    public List<String> characters() {
        return characters.stream()
                .map(character -> {
                    return "/api/v1/characters/" + character.getId();
                }).collect(Collectors.toList());
    }

    public void addCharacter(MovieCharacter character) {
        if (characters == null) {
            characters = new HashSet<>();
        }
        characters.add(character);
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getGenre() {
        return genre;
    }

    public Date getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(Date releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public Franchise getFranchise() {
        return franchise;
    }

    public void setFranchise(Franchise franchise) {
        this.franchise = franchise;
    }

    public Set<MovieCharacter> getCharacters() {
        return characters;
    }

    public void setCharacters(Set<MovieCharacter> characters) {
        this.characters = characters;
    }
}
