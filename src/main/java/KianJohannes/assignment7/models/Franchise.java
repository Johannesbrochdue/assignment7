package KianJohannes.assignment7.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    public Franchise(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Franchise() {
    }

    @OneToMany
    @JoinColumn(
        name = "franchise_id"
    )
    public Set<Movie> movies;

    public void addMovie(Movie movie) {
        if (movies == null) {
            movies = new HashSet<>();
        }
        movies.add(movie);
    }

    @JsonGetter("movies")
    public List<String> moviesGetter() {
        if (movies != null) {
            return movies.stream()
                .map(movie -> {
                    return "/api/v1/movies/" + movie.getId();
                }).collect(Collectors.toList());
        }
        return null;
    }

    public List<Movie> getMovies() {
        if (movies != null) {
            return new ArrayList<>(movies);
        }
        return null;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

