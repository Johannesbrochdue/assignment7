package KianJohannes.assignment7;

import KianJohannes.assignment7.models.Franchise;
import KianJohannes.assignment7.models.Movie;
import KianJohannes.assignment7.models.MovieCharacter;
import KianJohannes.assignment7.repositories.FranchiseRepository;
import KianJohannes.assignment7.repositories.MovieCharacterRepository;
import KianJohannes.assignment7.repositories.MovieRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class AppStartupRunner implements ApplicationRunner {
    private static final Logger logger = LoggerFactory.getLogger(AppStartupRunner.class);

    @Autowired
    MovieCharacterRepository movieCharacterRepository;

    @Autowired
    MovieRepository movieRepository;

    @Autowired
    FranchiseRepository franchiseRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        logger.info("Your application started with option names : {}", args.getOptionNames());
        if (movieRepository.count() == 0) {
            MovieCharacter bale = new MovieCharacter("Christian Bale", "Male", "Batman");
            MovieCharacter cruise = new MovieCharacter("Tom Cruise", "Male", "The guy in top gun");
            MovieCharacter johansen = new MovieCharacter("Scarlett Johansen", "Female", "The girl in lost in translation");
            Movie batman = new Movie("Batman", "Romance");
            Movie darkknight = new Movie("Batman: The Dark Knight", "Comedy");
            Movie topgun = new Movie("Top Gun", "Fantasy");
            Movie lostintranslation = new Movie("Lost in translation", "Sci-Fi");
            batman.addCharacter(bale);
            topgun.addCharacter(bale);
            topgun.addCharacter(cruise);
            lostintranslation.addCharacter(johansen);

            Franchise bat = new Franchise("Batman Movies", "Best movies in the world");
            bat.addMovie(batman);
            bat.addMovie(topgun);

            Franchise HBO = new Franchise("HBO", "Second best movies in the world");
            HBO.addMovie(lostintranslation);
            movieCharacterRepository.save(bale);
            movieCharacterRepository.save(cruise);
            movieCharacterRepository.save(johansen);
            movieRepository.save(batman);
            movieRepository.save(topgun);
            movieRepository.save(lostintranslation);

            franchiseRepository.save(bat);
            franchiseRepository.save(HBO);


        }
        System.out.println(movieRepository.findAll());
    }
}