package KianJohannes.assignment7.repositories;

import KianJohannes.assignment7.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FranchiseRepository extends JpaRepository<Franchise, Long> {
}
