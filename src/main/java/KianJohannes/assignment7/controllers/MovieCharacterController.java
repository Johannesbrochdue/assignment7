package KianJohannes.assignment7.controllers;

import KianJohannes.assignment7.models.Movie;
import KianJohannes.assignment7.models.MovieCharacter;
import KianJohannes.assignment7.repositories.MovieCharacterRepository;
import KianJohannes.assignment7.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/characters")
public class MovieCharacterController {
    @Autowired
    MovieCharacterRepository movieCharacterRepository;

    @Autowired
    MovieRepository movieRepository;

    @GetMapping()
    public ResponseEntity<List<MovieCharacter>> getAllCharacters() {
        List<MovieCharacter> movieCharacters = movieCharacterRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(movieCharacters, status);
    }

    @GetMapping("/{id}")
    public ResponseEntity<MovieCharacter> getMovieCharacterById(@PathVariable Long id) {
        HttpStatus status;
        MovieCharacter returnMovieCharacter = new MovieCharacter();
        if (movieCharacterRepository.existsById(id)) {
            status = HttpStatus.OK;
            returnMovieCharacter = movieCharacterRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnMovieCharacter, status);
    }

    @PostMapping
    public ResponseEntity<MovieCharacter> addMovieCharacter(@RequestBody MovieCharacter movie){
        HttpStatus status;
        movie = movieCharacterRepository.save(movie);
        status = HttpStatus.CREATED;
        return new ResponseEntity<>(movie, status);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> deleteMovieCharacter(@PathVariable Long id) {
        HttpStatus status;
        Boolean res = false;
        if (movieCharacterRepository.existsById(id)) {
            MovieCharacter toDelete = movieCharacterRepository.getById(id);
            Set<Movie> moviesForCharacter = toDelete.getMovies();
            for (Movie movie: moviesForCharacter) {
                movie.characters.remove(toDelete);
            }
            movieCharacterRepository.delete(toDelete);
            status = HttpStatus.OK;
            res = true;
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(res, status);
    }
}
