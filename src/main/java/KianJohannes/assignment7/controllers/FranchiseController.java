package KianJohannes.assignment7.controllers;

import KianJohannes.assignment7.models.Franchise;
import KianJohannes.assignment7.models.Movie;
import KianJohannes.assignment7.models.MovieCharacter;
import KianJohannes.assignment7.repositories.FranchiseRepository;
import KianJohannes.assignment7.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/franchises")
public class FranchiseController {

    @Autowired
    FranchiseRepository franchiseRepository;

    @GetMapping()
    public ResponseEntity<List<Franchise>> getAllFranchises() {
        List<Franchise> franchises = franchiseRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(franchises, status);
    }

    @GetMapping("/{id}")
    public ResponseEntity<List<Movie>> getAllMovies(@PathVariable Long id) {
        HttpStatus status;
        List<Movie> movies = null;
        if (franchiseRepository.existsById(id)) {
            status = HttpStatus.OK;
            movies = franchiseRepository.findById(id).get().getMovies();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(movies, status);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> deleteFranchise(@PathVariable Long id) {
        HttpStatus status;
        Boolean res = false;
        if (franchiseRepository.existsById(id)) {
            franchiseRepository.delete(franchiseRepository.getById(id));
            status = HttpStatus.OK;
            res = true;
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(res, status);
    }
}
